package com.gitlab.demo.implementinghexagonalarchitecture.springboot.boundry;


import com.gitlab.demo.implementinghexagonalarchitecture.springboot.boundry.driven_port.IObtainPoems;
import com.gitlab.demo.implementinghexagonalarchitecture.springboot.boundry.driven_port.IWriteLines;
import com.gitlab.demo.implementinghexagonalarchitecture.springboot.boundry.driver_port.IReactToCommands;
import com.gitlab.demo.implementinghexagonalarchitecture.springboot.boundry.internal.command_handler.DisplayRandomPoem;
import com.gitlab.demo.implementinghexagonalarchitecture.springboot.command.AskForPoem;
import lombok.extern.slf4j.Slf4j;

import java.util.function.Consumer;


@Slf4j
public class Boundary implements IReactToCommands {
    private final IObtainPoems poemObtainer;
    private final IWriteLines lineWriter;


    public Boundary(IObtainPoems poemObtainer, IWriteLines lineWriter) {
        this.poemObtainer = poemObtainer;
        this.lineWriter = lineWriter;
    }


    @Override
    public void reactTo(Object commandObject) {
        log.info("command:{}",commandObject);
        displaysRandomPoem();
    }

    private Consumer<AskForPoem> displaysRandomPoem() {
        return new DisplayRandomPoem(poemObtainer, lineWriter);
    }
}