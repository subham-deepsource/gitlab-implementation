package com.gitlab.demo.implementinghexagonalarchitecture.springboot.boundry.driven_port;

public interface IObtainPoems {
    String[] getMePoems(String language);
}