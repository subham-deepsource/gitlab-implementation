package com.gitlab.demo.implementinghexagonalarchitecture.springboot.boundry.driven_port;

public interface IWriteLines {
    void writeLines(String strings);
}
