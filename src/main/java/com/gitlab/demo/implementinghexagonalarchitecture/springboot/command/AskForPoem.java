package com.gitlab.demo.implementinghexagonalarchitecture.springboot.command;

/**
 * https://www.freecodecamp.org/news/implementing-a-hexagonal-architecture/
 */
public class AskForPoem {
    private String language;

    public AskForPoem(String language) {
        this.language = language;
    }

    public String getLanguage() {
        return language;
    }
}
