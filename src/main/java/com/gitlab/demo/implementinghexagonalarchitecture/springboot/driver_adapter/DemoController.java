package com.gitlab.demo.implementinghexagonalarchitecture.springboot.driver_adapter;

import com.gitlab.demo.implementinghexagonalarchitecture.springboot.boundry.SpringMvcBoundary;
import com.gitlab.demo.implementinghexagonalarchitecture.springboot.command.AskForPoem;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class DemoController {

    private SpringMvcBoundary springMvcBoundary;

    @Autowired
    public DemoController(SpringMvcBoundary springMvcBoundary) {
        this.springMvcBoundary = springMvcBoundary;
    }

    @GetMapping("/askForPoem")
    public String askForPoem(@RequestParam(name = "lang", required = false, defaultValue = "en") String language,
                             Model webModel) {
        springMvcBoundary.basedOn(webModel).reactTo(new AskForPoem(language));
        return "poemView";
    }
}
